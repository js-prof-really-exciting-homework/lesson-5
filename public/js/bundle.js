/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/hoc.js":
/*!****************************!*\
  !*** ./application/hoc.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\r\n/*\r\n\r\n  Higher Order Functions\r\n  function is a values\r\n\r\n*/\r\nlet hofDemo = () => {\r\n\r\n  let multiply = x => x * x;\r\n  let nine = multiply(3);\r\n  console.log( 'multiply:', nine );\r\n\r\n  /*\r\n\r\n    Array.filter (так же как map, forEach, etc...) пример использования HOF в нативном js\r\n    Паттерн позворялет использовать композцицию что бы собрать из маленьких функций одну большую\r\n\r\n  */\r\n\r\n  let zoo = [\r\n    {id:0, name:\"WoofMaker\", species: 'dog'},\r\n    {id:1, name:\"WhiteFurr\", species: 'rabbit'},\r\n    {id:2, name:\"MeowMaker\", species: 'cat'},\r\n    {id:3, name:\"PoopMaker\", species: 'dog'},\r\n    {id:4, name:\"ScratchMaker\", species: 'cat'},\r\n  ]\r\n\r\n  let isDog = animal => animal.species === 'dog';\r\n  let isCat = animal => animal.species === 'cat';\r\n\r\n  let dogs = zoo.filter( isDog );\r\n  let cats = zoo.filter( isCat );\r\n\r\n  console.log('Here dogs:', dogs);\r\n  console.log('Here cats:', cats);\r\n\r\n  // - - - - - - - - - - - - - - - - - -\r\n\r\n  function compose(func_a, func_b){\r\n    return function(c){\r\n      return func_a( func_b(c) );\r\n    }\r\n  }\r\n  const addTwo = value => {\r\n    console.log('Add', value);\r\n    return value + 2\r\n  }\r\n  const multiplyTwo = value => {\r\n    console.log('Mulitple', value);\r\n    return value * 2;\r\n  }\r\n\r\n  const addTwoAndMultiplayTwo = compose( addTwo, multiplyTwo );\r\n  // addTwoAndMultiplayTwo( 10 )\r\n\r\n  /*\r\n    В данном случае происходит следующее:\r\n    - Вызывается ф-я compose которая принимает ф-и addTwo, multiplyTwo как аргументы\r\n    - Вызывается функция которая передана как аргумент func_b\r\n    - Результат её выполнения передается в функция func_a\r\n    - Общий результат возвращается в ф-ю которая нам возвращается в переменную\r\n  */\r\n\r\n  // console.log(\r\n  //   addTwoAndMultiplayTwo(2),\r\n  //   addTwoAndMultiplayTwo(6),\r\n  //   addTwoAndMultiplayTwo(40)\r\n  // );\r\n\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (hofDemo);\r\n\n\n//# sourceURL=webpack:///./application/hoc.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observer */ \"./application/observer/index.js\");\n/* harmony import */ var _hoc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hoc */ \"./application/hoc.js\");\n/* harmony import */ var _classwork_customEvent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classwork/customEvent */ \"./classwork/customEvent.js\");\n/* harmony import */ var _classwork_observer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../classwork/observer */ \"./classwork/observer.js\");\n// Точка входа в наше приложение\r\n\r\n\r\n// import CustomEvents from './observer/CustomEvents';\r\n// import obs from '../classworks/observer';\r\n\r\n\r\n\r\n\r\n// 0. HOC\r\n// HOC();\r\n// 1. Observer ->\r\n// console.log( Observer );\r\n// Observer();\r\n// console.log('INDEX');\r\n// 2. CustomEvents ->\r\n// CustomEvents();\r\n\r\n//HOMEWORKS\r\n// customEventHW();\r\nObject(_classwork_observer__WEBPACK_IMPORTED_MODULE_3__[\"default\"])();\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/observer/Observer.js":
/*!******************************************!*\
  !*** ./application/observer/Observer.js ***!
  \******************************************/
/*! exports provided: Observable, Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observable\", function() { return Observable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observer\", function() { return Observer; });\nfunction Observable(){\r\n  // Создаем список подписаных обьектов\r\n  var observers = [];\r\n  // Оповещение всех подписчиков о сообщении\r\n  this.sendMessage = function( msg ){\r\n      observers.map( ( obs ) => {\r\n        obs.notify(msg);\r\n      });\r\n  };\r\n  // Добавим наблюдателя\r\n  this.addObserver = function( observer ){\r\n    observers.push( observer );\r\n  };\r\n}\r\n// Сам наблюдатель:\r\nfunction Observer( behavior ){\r\n  // Делаем функцию, что бы через callback можно\r\n  // было использовать различные функции внутри\r\n  this.notify = function( callback ){\r\n    behavior( callback );\r\n  };\r\n}\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./application/observer/Observer.js?");

/***/ }),

/***/ "./application/observer/demo1.js":
/*!***************************************!*\
  !*** ./application/observer/demo1.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/observer/Observer.js\");\n\r\n\r\nconst Demo1 = () => {\r\n  console.log( 'DEMO 1 ONLINE');\r\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\r\n  let obs1 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.log(msg));\r\n  let obs2 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.warn(msg));\r\n  let obs3 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.error(msg));\r\n  //\r\n  observable.addObserver( obs1 );\r\n  observable.addObserver( obs2 );\r\n  observable.addObserver( obs3 );\r\n  //\r\n  console.log( observable );\r\n  observable.sendMessage('hello');\r\n\r\n\r\n  //  Проверим абстрактно как оно работает:\r\n  // setTimeout(\r\n  //   ()=>{\r\n  //     // оправим сообщение, с текущей датой:\r\n  //     observable.sendMessage('Now is' + new Date());\r\n  //   }, 2000\r\n  // );\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo1);\r\n\n\n//# sourceURL=webpack:///./application/observer/demo1.js?");

/***/ }),

/***/ "./application/observer/demo2.js":
/*!***************************************!*\
  !*** ./application/observer/demo2.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/observer/Observer.js\");\n\r\n\r\nconst Demo2 = () => {\r\n\r\n  /*\r\n    Рассмотрим на примере интернет магазина:\r\n  */\r\n  console.log( 'DEMO 2');\r\n  let Products = [\r\n    {\r\n      id: 1,\r\n      name: 'Samsung Galaxy S8 ',\r\n      price: 21999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/1894533/samsung_galaxy_s8_64gb_black_images_1894533385.jpg'\r\n    },\r\n    {\r\n      id: 2,\r\n      name: 'Apple AirPort Capsule',\r\n      price: 10700,\r\n      imageLink: 'https://i1.rozetka.ua/goods/3330569/apple_a1470_me177_images_3330569615.jpg'\r\n    },\r\n    {\r\n      id: 3,\r\n      name: 'Apple iPhone X',\r\n      price: 35999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/2433231/apple_iphone_x_64gb_silver_images_2433231297.jpg'\r\n    },\r\n    {\r\n      id: 4,\r\n      name: 'LG G6 Black ',\r\n      price: 15999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/1892329/copy_lg_lgh845_acistn_58d8fc4a87d51_images_1892329834.jpg'\r\n    }\r\n  ];\r\n\r\n  // Создадим наблюдателя:\r\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\r\n  // Трех обсерверов:\r\n  let basketObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( function(id){\r\n    let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n        // console.log();\r\n        Cart.push( filtredToBasket[0] );\r\n        renderBasket();\r\n  });\r\n\r\n  let serverObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\r\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n      let msg = `Товар ${filtredToBasket[0].name} добавлен в корзину`;\r\n      console.log( msg );\r\n  });\r\n\r\n  let iconObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\r\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n\r\n      let products__cart = document.getElementById('products__cart');\r\n          products__cart.innerText = Cart.length;\r\n  });\r\n\r\n  observable.addObserver( basketObs );\r\n  observable.addObserver( serverObs );\r\n  observable.addObserver( iconObs );\r\n\r\n  // Render Data - - - - - - - - - - - -\r\n  let Cart = [];\r\n  let products__row = document.getElementById('products__row');\r\n\r\n  function renderBasket(){\r\n    let cartElem = document.getElementById('cart');\r\n    let message;\r\n        if( Cart.length === 0 ){\r\n          message = 'У вас в корзине пусто';\r\n        } else {\r\n          let Sum = Cart.reduce( (prev, current) => {\r\n            return prev += Number(current.price);\r\n          }, 0);\r\n          message = `У вас в корзине ${Cart.length} товаров, на сумму: ${Sum} грн.`;\r\n        }\r\n        cartElem.innerHTML = `<h2>${message}</h2><ol></ol>`;\r\n\r\n        let ol = cartElem.querySelector('ol');\r\n        Cart.map( item => {\r\n          let li = document.createElement('li');\r\n              li.innerText = `${item.name} (${item.price} грн.)`;\r\n              ol.appendChild(li);\r\n        });\r\n  }\r\n\r\n  Products.map( item => {\r\n    let product = document.createElement('div');\r\n        product.className = \"product\";\r\n        product.innerHTML =\r\n        `<div class=\"product__image\">\r\n            <img src=\"${item.imageLink}\"/>\r\n          </div>\r\n          <div class=\"product__name\">${item.name}</div>\r\n          <div class=\"product__price\">${item.price} грн.</div>\r\n          <div class=\"product__action\">\r\n            <button class=\"product__buy\" data-id=${item.id}> Купить </button>\r\n          </div>`;\r\n        let buyButton = product.querySelector('.product__buy');\r\n            buyButton.addEventListener('click', (e) => {\r\n              let id = e.target.dataset.id;\r\n              observable.sendMessage(id);\r\n            });\r\n        products__row.appendChild(product);\r\n  });\r\n\r\n  renderBasket();\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo2);\r\n\n\n//# sourceURL=webpack:///./application/observer/demo2.js?");

/***/ }),

/***/ "./application/observer/index.js":
/*!***************************************!*\
  !*** ./application/observer/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _demo1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./demo1 */ \"./application/observer/demo1.js\");\n/* harmony import */ var _demo2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./demo2 */ \"./application/observer/demo2.js\");\n\r\n\r\n\r\nconst ObserverDemo = () => {\r\n  // Abstract Demo 2\r\n  // console.log('demo 1');\r\n  // Demo1();\r\n\r\n  // Functional Demo:\r\n  // Demo2();\r\n\r\n}; //observer Demo\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObserverDemo);\r\n\n\n//# sourceURL=webpack:///./application/observer/index.js?");

/***/ }),

/***/ "./classwork/customEvent.js":
/*!**********************************!*\
  !*** ./classwork/customEvent.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в\r\n            соответсвии с правилавми ниже:\r\n\r\n  1. Написать кастомные события которые будут менять статус светофора:\r\n  - start: включает зеленый свет\r\n  - stop: включает красный свет\r\n  - night: включает желтый свет, который моргает с интервалом в 1с.\r\n  И зарегистрировать каждое через addEventListener на каждом из светофоров.\r\n\r\n  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,\r\n      чтобы включить режим \"нерегулируемого перекрестка\" (моргающий желтый).\r\n\r\n  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)\r\n      или зеленый (на второй клик) цвет соотвественно.\r\n      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.\r\n\r\n  4.  + Бонус: На кнопку \"Start Night\" повесить сброс всех светофоров с их текущего\r\n      статуса, на мигающий желтые.\r\n      Двойной, тройной и более клики на кнопку не должны вызывать повторную\r\n      инициализацию инвервала.\r\n\r\n*/\r\n// document.addEventListener('DOMContentloaded', () => {\r\n//   let trafficLights = document.querySelectorAll('.trafficLight');\r\n//   let trafficLightsArray = Array.from(trafficLights)\r\n//   trafficLightsArray.map(item => {\r\n//     console.log(item);\r\n//   })\r\n// });\r\n\r\nlet customEventHW = () => {\r\n\r\n  let start = new Event('greenLight', {\"bubbles\":true, \"cancelable\":false});\r\n  let stop = new Event('redLight', {\"bubbles\":true, \"cancelable\":false});\r\n  let night = new Event('yellowLight', {\"bubbles\":true, \"cancelable\":false});  \r\n\r\n// document.addEventListener('DOMContentloaded', () => {\r\n  let trafficLights = document.querySelectorAll('.trafficLight');\r\n  let trafficLightsArray = Array.from(trafficLights)\r\n  trafficLightsArray.map(item => {\r\n//Добавляем ивенты\r\n    item.addEventListener('greenLight', (e) => {\r\n      if (item.timer){\r\n        clearInterval(item.timer);\r\n      }\r\n        item.dataset.light = 'green';\r\n        let circle = item.querySelectorAll('.trafficLight__circle');\r\n        circle[0].style.backgroundColor = '';  \r\n        circle[1].style.backgroundColor = '';  \r\n        circle[2].style.backgroundColor = 'green';\r\n    });\r\n\r\n    item.addEventListener('redLight', (e) => {\r\n      if (item.timer){\r\n        clearInterval(item.timer);\r\n      }\r\n        e.target.dataset.light = 'red';\r\n        let circle = item.querySelectorAll('.trafficLight__circle');\r\n        circle[0].style.backgroundColor = 'red';\r\n        circle[1].style.backgroundColor = '';  \r\n        circle[2].style.backgroundColor = '';              \r\n    });\r\n\r\n    item.addEventListener('yellowLight', (e) => {\r\n        e.target.dataset.status = 'night';\r\n        let circle = item.querySelectorAll('.trafficLight__circle');\r\n        circle[0].style.backgroundColor = '';\r\n        circle[2].style.backgroundColor = '';\r\n        item.timer = setInterval(() => {\r\n          circle[1].style.backgroundColor = circle[1].style.backgroundColor === 'yellow' ? '' : 'yellow';\r\n        }, 1000);\r\n\r\n    });\r\n\r\n    item.addEventListener('click', (e) => {\r\n      if (e.target.dataset.light !== 'red'){\r\n        item.dispatchEvent(stop);\r\n      } else if (e.target.dataset.light === 'red'){\r\n        item.dispatchEvent(start);\r\n      }\r\n    });\r\n\r\n    //Night on loading\r\n\r\n    window.addEventListener('load', () => {\r\n      item.dispatchEvent(night);\r\n    })\r\n  })\r\n\r\n  //Start Night Button\r\n  let btn = document.getElementById('Do');\r\n      btn.addEventListener('click', () => {\r\n        trafficLightsArray.map(item => {\r\n          if (item.dataset.status === 'night'){\r\n            clearInterval(item.timer);\r\n            item.dataset.status = 'day';\r\n            let circle = item.querySelectorAll('.trafficLight__circle');\r\n            let circleArr = Array.from(circle);\r\n            circleArr.map(item => item.style.backgroundColor = '');\r\n          } else {\r\n            item.dispatchEvent(night);            \r\n          }         \r\n        });\r\n      });\r\n// });\r\n\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (customEventHW);\n\n//# sourceURL=webpack:///./classwork/customEvent.js?");

/***/ }),

/***/ "./classwork/observer.js":
/*!*******************************!*\
  !*** ./classwork/observer.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _application_observer_Observer_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/observer/Observer.js */ \"./application/observer/Observer.js\");\n/*\r\n  Задание: Модуль создания плейлиста, используя паттерн Обсервер.\r\n\r\n  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:\r\n    1. Список исполнителей и песен (Находится слева) - отуда можно включить\r\n    песню в исполнение иди добавить в плейлист.\r\n    Если песня уже есть в плейлисте, дважды добавить её нельзя.\r\n\r\n    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,\r\n    или запустить в исполнение. Внизу списка должен выводиться блок, в котором\r\n    пишет суммарное время проигрывания всех песен в плейлисте.\r\n\r\n    3. Отображает песню которая проигрывается.\r\n\r\n    4. + Бонус: Сделать прогресс пар того как проигрывается песня\r\n    с возможностью его остановки.\r\n*/\r\n\r\n\r\n\r\nconst MusicList = [\r\n  {\r\n    title: 'Rammstein',\r\n    songs: [\r\n      {\r\n        id: 1,\r\n        name: 'Du Hast',\r\n        time: [3, 12]\r\n      },\r\n      {\r\n        id: 2,\r\n        name: 'Ich Will',\r\n        time: [5, 10]\r\n      },\r\n      {\r\n        id: 3,\r\n        name: 'Mutter',\r\n        time: [4, 15]\r\n      },\r\n      {\r\n        id: 4,\r\n        name: 'Ich tu dir weh',\r\n        time: [5, 13]\r\n      },\r\n      {\r\n        id: 5,\r\n        name: 'Rammstein',\r\n        time: [3, 54]\r\n      }\r\n    ]\r\n  },\r\n  {\r\n    title: 'System of a Down',\r\n    songs: [\r\n      {\r\n        id: 6,\r\n        name: 'Toxicity',\r\n        time: [4, 22]\r\n      },\r\n      {\r\n        id: 7,\r\n        name: 'Sugar',\r\n        time: [2, 44]\r\n      },\r\n      {\r\n        id: 8,\r\n        name: 'Lonely Day',\r\n        time: [3, 19]\r\n      },\r\n      {\r\n        id: 9,\r\n        name: 'Lost in Hollywood',\r\n        time: [5, 9]\r\n      },\r\n      {\r\n        id: 10,\r\n        name: 'Chop Suey!',\r\n        time: [2, 57]\r\n      }\r\n    ]\r\n  },\r\n  {\r\n    title: 'Green Day',\r\n    songs: [\r\n      {\r\n        id: 11,\r\n        name: '21 Guns',\r\n        time: [4, 16]\r\n      },\r\n      {\r\n        id: 12,\r\n        name: 'Boulevard of broken dreams!',\r\n        time: [6, 37]\r\n      },\r\n      {\r\n        id: 13,\r\n        name: 'Basket Case!',\r\n        time: [3, 21]\r\n      },\r\n      {\r\n        id: 14,\r\n        name: 'Know Your Enemy',\r\n        time: [4, 11]\r\n      }\r\n    ]\r\n  },\r\n  {\r\n    title: 'Linkin Park',\r\n    songs: [\r\n      {\r\n        id: 15,\r\n        name: 'Numb',\r\n        time: [3, 11]\r\n      },\r\n      {\r\n        id: 16,\r\n        name: 'New Divide',\r\n        time: [4, 41]\r\n      },\r\n      {\r\n        id: 17,\r\n        name: 'Breaking the Habit',\r\n        time: [4, 1]\r\n      },\r\n      {\r\n        id: 18,\r\n        name: 'Faint',\r\n        time: [3, 29]\r\n      }\r\n    ]\r\n  }\r\n]\r\n\r\nconst MusicBoxHW = () => {\r\n  let MusicPlaying = document.getElementById('MusicPlaying');\r\n  let MusicPlayList = document.getElementById('MusicPlayList');\r\n  let playlist = [];  \r\n  //========MODAL WINDOW==========\r\n  let modal = document.getElementById('myModal');\r\n  let span = document.getElementsByClassName(\"close\")[0];\r\n  // When the user clicks on <span> (x), close the modal\r\n  span.addEventListener('click', function() {\r\n    modal.style.display = \"none\";\r\n  });\r\n\r\n// When the user clicks anywhere outside of the modal, close it\r\nwindow.addEventListener('click', function(event) {\r\n  if (event.target == modal) {\r\n    modal.style.display = \"none\";\r\n  }\r\n});\r\n  \r\n  //=========OBSERVERS============\r\n  let observable = new _application_observer_Observer_js__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();  \r\n  let observerAddToPL = new _application_observer_Observer_js__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"](id => {      \r\n  MusicList.map(Artist => {  \r\n      Artist.songs.map(song => {\r\n        if (Number(song.id) === Number(id)){\r\n          playlist.push(song);\r\n        }\r\n      });\r\n    });    \r\n    player.playlistRender();\r\n  });\r\n\r\n  let observerShowPopup = new _application_observer_Observer_js__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"](id => {\r\n    let par = document.getElementById('modalText');\r\n      MusicList.map(Artist => {  \r\n      Artist.songs.map(song => {\r\n        if (Number(song.id) === Number(id)){\r\n          par.innerHTML = `You have added <b>${song.name}</b> to your playlist`;\r\n        }\r\n      });\r\n    });\r\n    modal.style.display = \"block\";\r\n  });\r\n\r\n  let observerCountDuration = new _application_observer_Observer_js__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]()\r\n\r\n  observable.addObserver(observerAddToPL);\r\n  observable.addObserver(observerShowPopup);\r\n\r\n//===========PLayer class ==============\r\nclass Player {\r\n  constructor(){\r\n    this.render = this.render.bind(this);\r\n    this.playlistRender = this.playlistRender.bind(this);\r\n    this.activeRender = this.activeRender.bind(this);\r\n    this.removeSong = this.removeSong.bind(this);\r\n  }\r\n\r\n  render(){\r\n    const MusicBox = document.getElementById('MusicBox');  \r\n    MusicList.map( Artist => {\r\n      const div = document.createElement('div');\r\n      div.innerHTML = `<h4>${Artist.title}</h4>`;\r\n      Artist.songs.map( song => {\r\n        const ul = document.createElement('ul');\r\n              ul.innerHTML += `<li>${song.name} <button class=\"play\" data-id=\"${song.id}\">&#x25b6;</button> <button class=\"add\" data-id=\"${song.id}\">&#x2b;</button></li>`;\r\n              div.appendChild(ul);\r\n        let play = div.querySelector(`.play[data-id=\"${song.id}\"]`);        \r\n            play.addEventListener('click', this.activeRender);\r\n\r\n            //==========Adding to Playlist==============\r\n        let add = div.querySelector(`.add[data-id=\"${song.id}\"]`);\r\n            add.addEventListener('click', e => {              \r\n              let id = e.target.dataset.id;              \r\n              observable.sendMessage(id);     //Sending message to observers  \r\n            });\r\n      })\r\n      MusicBox.appendChild(div);\r\n    });    \r\n  }\r\n\r\n\r\n  playlistRender(){\r\n    let duration = 0;\r\n    let time = [0, 0];\r\n    \r\n    MusicPlayList.innerHTML = '';\r\n\r\n    playlist.map(song => {\r\n      let block = document.createElement('div');\r\n          block.innerHTML = `<b>${song.name}</b>, ${song.time[0]}:${song.time[1]} <button class=\"remove\" data-id='${song.id}'>X</button><br />`;\r\n          MusicPlayList.appendChild(block);\r\n\r\n      let remove = document.querySelector(`.remove[data-id=\"${song.id}\"]`);\r\n          remove.addEventListener('click', this.removeSong);\r\n\r\n          duration += (Number(song.time[0])*60) + Number(song.time[1]);\r\n      });\r\n\r\n    if (playlist.length !== 0){\r\n      let par = document.createElement('p');\r\n      time[0] = Math.floor(duration/60);\r\n      time[1] = Math.floor(duration - time[0]*60);      \r\n      par.innerHTML = `Продолжительность: ${time[0]}:${time[1]}`\r\n      MusicPlayList.appendChild(par);\r\n    }\r\n  }\r\n\r\n  removeSong(e){    \r\n    let id = e.target.dataset.id;\r\n\r\n    playlist.map(song => {\r\n      if (Number(song.id) === Number(id)){\r\n        let idx = playlist.indexOf(song);\r\n        playlist.splice(idx,1);\r\n      }\r\n    });\r\n\r\n    this.playlistRender();\r\n  }\r\n\r\n  //==========RENDER SONG PLAYING NOW===========\r\n  activeRender(e){\r\n    let id = e.target.dataset.id;\r\n    MusicList.map(Artist => {\r\n      Artist.songs.map(song => {\r\n        if (Number(id) === Number(song.id)){\r\n          MusicPlaying.innerHTML =\r\n            `\r\n              <div class=\"song__name\">${song.name}</div>\r\n              <div class=\"song__creator\">${Artist.title}</div>\r\n              <div class=\"song__duration\">${song.time[0]}:${song.time[1]}</div>\r\n              <div class=\"song__duration\" id=\"song_progress\"></div>\r\n              <button id='pause' data-status=\"playing\">&#10074;&#10074;</button>           \r\n            `;\r\n          let btn = document.getElementById('pause');\r\n              btn.addEventListener('click', e => {\r\n                if (e.target.dataset.status === 'playing'){\r\n                  btn.innerHTML = '&#x25b6;';\r\n                  e.target.dataset.status = 'paused'\r\n                  if (MusicPlaying.timer){\r\n                    clearInterval(MusicPlaying.timer);\r\n                  }\r\n                } else if (e.target.dataset.status === 'paused'){\r\n                  btn.innerHTML = '&#10074;&#10074;';\r\n                  e.target.dataset.status = 'playing';\r\n                  progressbar.start();\r\n                }\r\n\r\n              });\r\n          MusicPlaying.appendChild(btn);\r\n          let progressbar = new progressBar(song.time[0], song.time[1]);\r\n          progressbar.start();\r\n        }\r\n      });\r\n    });        \r\n  }\r\n}\r\n\r\n  let player = new Player;\r\n  player.render();\r\n\r\n  class progressBar {\r\n    constructor(min, sec){\r\n      this.length = (Number(min)*60) + Number(sec);\r\n      this.value = this.length;\r\n      this.start = this.start.bind(this);\r\n      this.render = this.render.bind(this);\r\n    }\r\n\r\n    render(){\r\n      let block = document.getElementById('song_progress');      \r\n      let template =\r\n        `\r\n          <progress max='${this.length}' value='${this.value}'>\r\n          </progress>\r\n        `\r\n      block.innerHTML = template;  \r\n    }\r\n\r\n    start(){\r\n    if (MusicPlaying.timer){\r\n      clearInterval(MusicPlaying.timer);\r\n    }      \r\n      MusicPlaying.timer = setInterval(() => {\r\n        if (this.value !== 0){\r\n          this.value--  \r\n        } else if (this.value === 0) {\r\n          clearInterval(MusicPlaying.timer)\r\n        }\r\n        this.render();\r\n      },1000)\r\n\r\n    }\r\n  }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (MusicBoxHW);\r\n\n\n//# sourceURL=webpack:///./classwork/observer.js?");

/***/ })

/******/ });