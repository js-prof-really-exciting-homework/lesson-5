/*
  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
            соответсвии с правилавми ниже:

  1. Написать кастомные события которые будут менять статус светофора:
  - start: включает зеленый свет
  - stop: включает красный свет
  - night: включает желтый свет, который моргает с интервалом в 1с.
  И зарегистрировать каждое через addEventListener на каждом из светофоров.

  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
      чтобы включить режим "нерегулируемого перекрестка" (моргающий желтый).

  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)
      или зеленый (на второй клик) цвет соотвественно.
      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

  4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
      статуса, на мигающий желтые.
      Двойной, тройной и более клики на кнопку не должны вызывать повторную
      инициализацию инвервала.

*/
// document.addEventListener('DOMContentloaded', () => {
//   let trafficLights = document.querySelectorAll('.trafficLight');
//   let trafficLightsArray = Array.from(trafficLights)
//   trafficLightsArray.map(item => {
//     console.log(item);
//   })
// });

let customEventHW = () => {

  let start = new Event('greenLight', {"bubbles":true, "cancelable":false});
  let stop = new Event('redLight', {"bubbles":true, "cancelable":false});
  let night = new Event('yellowLight', {"bubbles":true, "cancelable":false});  

// document.addEventListener('DOMContentloaded', () => {
  let trafficLights = document.querySelectorAll('.trafficLight');
  let trafficLightsArray = Array.from(trafficLights)
  trafficLightsArray.map(item => {
//Добавляем ивенты
    item.addEventListener('greenLight', (e) => {
      if (item.timer){
        clearInterval(item.timer);
      }
        item.dataset.light = 'green';
        let circle = item.querySelectorAll('.trafficLight__circle');
        circle[0].style.backgroundColor = '';  
        circle[1].style.backgroundColor = '';  
        circle[2].style.backgroundColor = 'green';
    });

    item.addEventListener('redLight', (e) => {
      if (item.timer){
        clearInterval(item.timer);
      }
        e.target.dataset.light = 'red';
        let circle = item.querySelectorAll('.trafficLight__circle');
        circle[0].style.backgroundColor = 'red';
        circle[1].style.backgroundColor = '';  
        circle[2].style.backgroundColor = '';              
    });

    item.addEventListener('yellowLight', (e) => {
        e.target.dataset.status = 'night';
        let circle = item.querySelectorAll('.trafficLight__circle');
        circle[0].style.backgroundColor = '';
        circle[2].style.backgroundColor = '';
        item.timer = setInterval(() => {
          circle[1].style.backgroundColor = circle[1].style.backgroundColor === 'yellow' ? '' : 'yellow';
        }, 1000);

    });

    item.addEventListener('click', (e) => {
      if (e.target.dataset.light !== 'red'){
        item.dispatchEvent(stop);
      } else if (e.target.dataset.light === 'red'){
        item.dispatchEvent(start);
      }
    });

    //Night on loading

    window.addEventListener('load', () => {
      item.dispatchEvent(night);
    })
  })

  //Start Night Button
  let btn = document.getElementById('Do');
      btn.addEventListener('click', () => {
        trafficLightsArray.map(item => {
          if (item.dataset.status === 'night'){
            clearInterval(item.timer);
            item.dataset.status = 'day';
            let circle = item.querySelectorAll('.trafficLight__circle');
            let circleArr = Array.from(circle);
            circleArr.map(item => item.style.backgroundColor = '');
          } else {
            item.dispatchEvent(night);            
          }         
        });
      });
// });

}

export default customEventHW;